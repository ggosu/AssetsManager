﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DA_AssetsManager_AssetsGroup.h"
#include "Engine/StreamableManager.h"
#include "I_AssetsManager.h"
#include "DA_AssetsManager.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FStructSubscriberInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int assetGroupIndex;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString assetName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UObject* subscriber = nullptr;
	
	 bool operator==(FStructSubscriberInfo a) const
	 {
		 if (a.assetGroupIndex == assetGroupIndex && a.assetName == assetName && a.subscriber == subscriber) 
		 	return true; 
		 else 
		 	return false;
	 }
	
};

USTRUCT(BlueprintType)
struct FStructAssetInfo_SK
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FStructSubscriberInfo> subscribers;	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USkeletalMesh* hardReference = nullptr;
	
};

USTRUCT(BlueprintType)
struct FStructAssetInfo_SM
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FStructSubscriberInfo> subscribers;	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* hardReference = nullptr;
	
};

USTRUCT(BlueprintType)
struct FStructAssetInfo_SW
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FStructSubscriberInfo> subscribers;	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundWave* hardReference = nullptr;
	
};

USTRUCT(BlueprintType)
struct FStructAssetInfo_TE
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FStructSubscriberInfo> subscribers;	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* hardReference = nullptr;
	
};


USTRUCT(BlueprintType)
struct FStructAssetInfo_Loader
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int assetTypeID;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int assetGroupIndex;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString assetName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<USkeletalMesh> lSoftRef_SK;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UStaticMesh> lSoftRef_SM;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<USoundWave> lSoftRef_SW;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSoftObjectPtr<UTexture2D> lSoftRef_TE;


};


UCLASS()
class ASSETSMANAGER_API UDA_AssetsManager : public UPrimaryDataAsset, public II_AssetsManager
{
	GENERATED_BODY()
	public:
	FStreamableManager assetLoader;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UDA_AssetsManager_AssetsGroup*> assetGroups;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<TSoftObjectPtr<USkeletalMesh>> SoftRef_SK;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<TSoftObjectPtr<UStaticMesh>> SoftRef_SM;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<TSoftObjectPtr<USoundWave>> SoftRef_SW;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<TSoftObjectPtr<UTexture2D>> SoftRef_TE;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<FStructAssetInfo_SK> AssetInfo_SK;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<FStructAssetInfo_SM> AssetInfo_SM;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<FStructAssetInfo_SW> AssetInfo_SW;
	
	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPROPERTY()
	TArray<FStructAssetInfo_TE> AssetInfo_TE;

	//Internal Functions
	UFUNCTION()
	void print(const FString& msg);

	
	
	UFUNCTION()
	FStructAssetInfo_SK genAssetInfo_SK(UObject* subscriber, int assetGroupIndex, const FString& assetName);
	UFUNCTION()
	FStructAssetInfo_SM genAssetInfo_SM(UObject* subscriber, int assetGroupIndex, const FString& assetName);
	UFUNCTION()
	FStructAssetInfo_SW genAssetInfo_SW(UObject* subscriber, int assetGroupIndex, const FString& assetName);
	UFUNCTION()
	FStructAssetInfo_TE genAssetInfo_TE(UObject* subscriber, int assetGroupIndex, const FString& assetName);
	
	//UFUNCTION()
	FStructAssetInfo_Loader genAssetInfo_Loader(TSoftObjectPtr<USkeletalMesh> softReference_SK, int assetGroupIndex, int assetTypeID, const FString& assetName);
	//UFUNCTION()
	FStructAssetInfo_Loader genAssetInfo_Loader(TSoftObjectPtr<UStaticMesh> softReference_SM, int assetGroupIndex, int assetTypeID, const FString& assetName);
	//UFUNCTION()
	FStructAssetInfo_Loader genAssetInfo_Loader(TSoftObjectPtr<USoundWave> softReference_SW, int assetGroupIndex, int assetTypeID, const FString& assetName);
	FStructAssetInfo_Loader genAssetInfo_Loader(TSoftObjectPtr<UTexture2D> softReference_TE, int assetGroupIndex, int assetTypeID, const FString& assetName);
	
	UFUNCTION()
	FStructSubscriberInfo genSubscriberInfo(UObject* subscriber, int assetGroupIndex, const FString& assetName);

	UFUNCTION()
	void RAM_load(UObject* subscriber, TArray<FStructAssetInfo_Loader> assetInfos);
	

	//Delegates
	UFUNCTION()
	void OnAssetAsyncLoadComplete(UObject* subscriber, TArray<FStructAssetInfo_Loader> assetInfos);
	
	//Interfaces
	virtual void RequestAssets_Implementation(UObject* subscriber, int assetTypeID, int assetGroupIndex, const TArray<FString>& assetName) override;
	virtual USkeletalMesh* GetAsset_SK_Implementation(UObject* subscriber, int assetGroupIndex, const FString& assetName) override;
	virtual UStaticMesh* GetAsset_SM_Implementation(UObject* subscriber, int assetGroupIndex, const FString& assetName) override;
	virtual USoundWave* GetAsset_SW_Implementation(UObject* subscriber, int assetGroupIndex, const FString& assetName) override;
	virtual UTexture2D* GetAsset_TE_Implementation(UObject* subscriber, int assetGroupIndex, const FString& assetName) override;

	
	virtual void RemoveAsset_Implementation(UObject* subscriber, int assetTypeID, int assetGroupIndex, const FString& assetName) override;
	virtual void ActorDestroy_Implementation(AActor* subscriber) override;
};
