﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DA_AssetsManager_AssetsGroup.generated.h"

/**
 * 
 */
UCLASS()
class ASSETSMANAGER_API UDA_AssetsManager_AssetsGroup : public UDataAsset
{
	GENERATED_BODY()
	public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (TitleProperty = "type"))
	TMap<FString,TSoftObjectPtr<USkeletalMesh>> SkeletalMeshes;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (TitleProperty = "type"))
	TMap<FString,TSoftObjectPtr<UStaticMesh>> StaticMeshes;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (TitleProperty = "type"))
	TMap<FString,TSoftObjectPtr<USoundWave>> SoundWaves;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (TitleProperty = "type"))
	TMap<FString,TSoftObjectPtr<UTexture2D>> Textures2D;
};
