﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "I_AssetsManager.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UI_AssetsManager : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ASSETSMANAGER_API II_AssetsManager
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	//assetTypeID:
	//0 = SkeletalMesh, 1 = StaticMesh, 2 = SoundWave 3 = Texture2D
	void RequestAssets(UObject* subscriber, int assetTypeID, int assetGroupIndex, const TArray<FString> &assetName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	//BE SURE YOU HAVE CLEARED/UNLINKED ASSETS BEFORE CALL IT!
	//OTHERWISE IT WILL STILL IN RAM
	//assetTypeID:
	//0 = SkeletalMesh, 1 = StaticMesh, 2 = SoundWave 3 = Texture2D
	void RemoveAsset(UObject* subscriber, int assetTypeID, int assetGroupIndex, const FString &assetName);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	USkeletalMesh* GetAsset_SK(UObject* subscriber, int assetGroupIndex, const FString &assetName);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	UStaticMesh* GetAsset_SM(UObject* subscriber, int assetGroupIndex, const FString &assetName);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	USoundWave* GetAsset_SW(UObject* subscriber, int assetGroupIndex, const FString &assetName);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	UTexture2D* GetAsset_TE(UObject* subscriber, int assetGroupIndex, const FString &assetName);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| From System")
	void AssetLoadedToMemory(int assetTypeID, int assetGroupIndex, const FString &assetName);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Assets Manager| CPP Interface| To System")
	void ActorDestroy(AActor* subscriber);
};
