﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "DA_AssetsManager.h"


void UDA_AssetsManager::print(const FString& msg)
{
	if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, msg);}
}

FStructAssetInfo_SK UDA_AssetsManager::genAssetInfo_SK(UObject* subscriber, int assetGroupIndex, const FString& assetName)
{
	FStructAssetInfo_SK assetInfo;
	FStructSubscriberInfo subscriberInfo = genSubscriberInfo(subscriber, assetGroupIndex, assetName);
	
	assetInfo.subscribers.Add(subscriberInfo);
	assetInfo.hardReference = nullptr;

	return assetInfo;
}

FStructAssetInfo_SM UDA_AssetsManager::genAssetInfo_SM(UObject* subscriber, int assetGroupIndex,
	const FString& assetName)
{
	FStructAssetInfo_SM assetInfo;
	FStructSubscriberInfo subscriberInfo = genSubscriberInfo(subscriber, assetGroupIndex, assetName);
	
	assetInfo.subscribers.Add(subscriberInfo);
	assetInfo.hardReference = nullptr;

	return assetInfo;
}

FStructAssetInfo_SW UDA_AssetsManager::genAssetInfo_SW(UObject* subscriber, int assetGroupIndex,
	const FString& assetName)
{
	FStructAssetInfo_SW assetInfo;
	FStructSubscriberInfo subscriberInfo = genSubscriberInfo(subscriber, assetGroupIndex, assetName);
	
	assetInfo.subscribers.Add(subscriberInfo);
	assetInfo.hardReference = nullptr;

	return assetInfo;
}

FStructAssetInfo_TE UDA_AssetsManager::genAssetInfo_TE(UObject* subscriber, int assetGroupIndex,
	const FString& assetName)
{
	FStructAssetInfo_TE assetInfo;
	FStructSubscriberInfo subscriberInfo = genSubscriberInfo(subscriber, assetGroupIndex, assetName);
	
	assetInfo.subscribers.Add(subscriberInfo);
	assetInfo.hardReference = nullptr;

	return assetInfo;
}

FStructAssetInfo_Loader UDA_AssetsManager::genAssetInfo_Loader(TSoftObjectPtr<USkeletalMesh> softReference_SK, int assetGroupIndex, int assetTypeID, const FString& assetName)
{
	FStructAssetInfo_Loader assetInfoLoader;
	//assetInfoLoader.subscriber = subscriber;
	assetInfoLoader.assetName = assetName;
	assetInfoLoader.assetGroupIndex = assetGroupIndex;
	assetInfoLoader.assetTypeID = assetTypeID;
	assetInfoLoader.lSoftRef_SK = softReference_SK;
	return assetInfoLoader;
}

FStructAssetInfo_Loader UDA_AssetsManager::genAssetInfo_Loader(TSoftObjectPtr<UStaticMesh> softReference_SM, int assetGroupIndex, int assetTypeID, const FString& assetName)
{
	FStructAssetInfo_Loader assetInfoLoader;
	//assetInfoLoader.subscriber = subscriber;
	assetInfoLoader.assetName = assetName;
	assetInfoLoader.assetGroupIndex = assetGroupIndex;
	assetInfoLoader.assetTypeID = assetTypeID;
	assetInfoLoader.lSoftRef_SM = softReference_SM;
	return assetInfoLoader;	
}

FStructAssetInfo_Loader UDA_AssetsManager::genAssetInfo_Loader(TSoftObjectPtr<USoundWave> softReference_SW, int assetGroupIndex, int assetTypeID, const FString& assetName)
{
	FStructAssetInfo_Loader assetInfoLoader;
	//assetInfoLoader.subscriber = subscriber;
	assetInfoLoader.assetName = assetName;
	assetInfoLoader.assetGroupIndex = assetGroupIndex;
	assetInfoLoader.assetTypeID = assetTypeID;
	assetInfoLoader.lSoftRef_SW = softReference_SW;
	return assetInfoLoader;	
}

FStructAssetInfo_Loader UDA_AssetsManager::genAssetInfo_Loader(TSoftObjectPtr<UTexture2D> softReference_TE, int assetGroupIndex, int assetTypeID, const FString& assetName)
{
	FStructAssetInfo_Loader assetInfoLoader;
	//assetInfoLoader.subscriber = subscriber;
	assetInfoLoader.assetName = assetName;
	assetInfoLoader.assetGroupIndex = assetGroupIndex;
	assetInfoLoader.assetTypeID = assetTypeID;
	assetInfoLoader.lSoftRef_TE = softReference_TE;
	return assetInfoLoader;	
}


FStructSubscriberInfo UDA_AssetsManager::genSubscriberInfo(UObject* subscriber, int assetGroupIndex, const FString& assetName)
{
	FStructSubscriberInfo subscriberInfo;
	
    subscriberInfo.assetGroupIndex = assetGroupIndex;
    subscriberInfo.assetName = assetName;
    subscriberInfo.subscriber = subscriber;
	
    return subscriberInfo;
}

void UDA_AssetsManager::RAM_load(UObject* subscriber, TArray<FStructAssetInfo_Loader> assetInfos)
{
	TArray<FSoftObjectPath> assetsToLoad;

	for (auto element : assetInfos)
	{
		switch (element.assetTypeID)
		{
		default:
			{
				break;
			}
		case 0:
			{
				assetsToLoad.AddUnique(element.lSoftRef_SK.ToSoftObjectPath());
				break;
			}
		case 1:
			{
				assetsToLoad.AddUnique(element.lSoftRef_SM.ToSoftObjectPath());
				break;
			}
		case 2:
			{
				assetsToLoad.AddUnique(element.lSoftRef_SW.ToSoftObjectPath());
				break;
			}
		case 3:
			{
				assetsToLoad.AddUnique(element.lSoftRef_TE.ToSoftObjectPath());
				break;
			}
		}	
	}
	

	assetLoader.RequestAsyncLoad(assetsToLoad, FStreamableDelegate::CreateUObject(this, &UDA_AssetsManager::OnAssetAsyncLoadComplete, subscriber, assetInfos));
}

void UDA_AssetsManager::OnAssetAsyncLoadComplete(UObject* subscriber, TArray<FStructAssetInfo_Loader> assetInfos)
{
	
	for (auto i : assetInfos)
	{		
			switch (i.assetTypeID)
			{
			default:
				{
					break;
				}
			case 0:
				{
					auto l_index = SoftRef_SK.Find((i.lSoftRef_SK));
					AssetInfo_SK[l_index].hardReference = Cast<USkeletalMesh>(SoftRef_SK[l_index].Get());									
					II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, i.assetTypeID, i.assetGroupIndex, i.assetName);					
					break;
				}
			case 1:
				{
					auto l_index = SoftRef_SM.Find((i.lSoftRef_SM));
					AssetInfo_SM[l_index].hardReference = Cast<UStaticMesh>(SoftRef_SM[l_index].Get());									
					II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, i.assetTypeID, i.assetGroupIndex, i.assetName);					
					break;
				}
			case 2:
				{
					auto l_index = SoftRef_SW.Find((i.lSoftRef_SW));
					AssetInfo_SW[l_index].hardReference = Cast<USoundWave>(SoftRef_SW[l_index].Get());									
					II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, i.assetTypeID, i.assetGroupIndex, i.assetName);					
					break;
				}
			case 3:
				{
					auto l_index = SoftRef_TE.Find((i.lSoftRef_TE));
					AssetInfo_TE[l_index].hardReference = Cast<UTexture2D>(SoftRef_TE[l_index].Get());									
					II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, i.assetTypeID, i.assetGroupIndex, i.assetName);					
					break;
				}
			}
	}

}


void UDA_AssetsManager::RequestAssets_Implementation(UObject* subscriber, int assetTypeID, int assetGroupIndex, const TArray<FString>& assetName)
{
	if (assetName.Num() <= 0 || !IsValid(subscriber))
	{
		return;
	}
	
	TArray<FStructAssetInfo_Loader> assetInfos;
	UDA_AssetsManager_AssetsGroup* groupToWorkWith = assetGroups[assetGroupIndex];

	switch (assetTypeID)
	{
	default:
		{
			break;
		}
	case 0:
		{
			for (int i = 0; i < assetName.Num(); ++i)
			{
				if (assetName[i].IsEmpty())
				{
					break;
				}
				if(groupToWorkWith->SkeletalMeshes.Find(assetName[i]))
				{
					const auto skeletSoftRef = groupToWorkWith->SkeletalMeshes.FindRef(assetName[i]);
					auto l_index = SoftRef_SK.Find(skeletSoftRef);
					if (!SoftRef_SK.Contains(skeletSoftRef))
					{
						AssetInfo_SK.Add(genAssetInfo_SK(subscriber, assetGroupIndex, assetName[i]));
						SoftRef_SK.Add(skeletSoftRef);
						assetInfos.Add(genAssetInfo_Loader(skeletSoftRef, assetGroupIndex, assetTypeID, assetName[i]));
					}
					else
					{
						auto newSub = genSubscriberInfo(subscriber, assetGroupIndex, assetName[i]);
						if(!AssetInfo_SK[l_index].subscribers.Contains(newSub))
						{
							AssetInfo_SK[l_index].subscribers.Add(newSub);
						}
						if(AssetInfo_SK[l_index].hardReference)
						{
							int lType = 0;
							FString l_AssetName = assetName[i];							
							II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, lType, assetGroupIndex, l_AssetName);
						}
						else
						{
							assetInfos.Add(genAssetInfo_Loader(skeletSoftRef, assetGroupIndex, assetTypeID, assetName[i]));							
						}
					}
				}
			}
			break;
		}
	case 1:
		{
			for (int i = 0; i < assetName.Num(); ++i)
			{
				if (assetName[i].IsEmpty())
				{
					break;
				}
				if(groupToWorkWith->StaticMeshes.Find(assetName[i]))
				{
					const auto staticSoftRef = groupToWorkWith->StaticMeshes.FindRef(assetName[i]);
					auto l_index = SoftRef_SM.Find(staticSoftRef);
					if (!SoftRef_SM.Contains(staticSoftRef))
					{
						AssetInfo_SM.Add(genAssetInfo_SM(subscriber, assetGroupIndex, assetName[i]));
						SoftRef_SM.Add(staticSoftRef);
						assetInfos.Add(genAssetInfo_Loader(staticSoftRef, assetGroupIndex, assetTypeID, assetName[i]));
					}
					else
					{
						auto newSub = genSubscriberInfo(subscriber, assetGroupIndex, assetName[i]);
						if(!AssetInfo_SM[l_index].subscribers.Contains(newSub))
						{
							AssetInfo_SM[l_index].subscribers.Add(newSub);
						}
						if(AssetInfo_SM[l_index].hardReference)
						{
							int lType = 0;
							FString l_AssetName = assetName[i];							
							II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, lType, assetGroupIndex, l_AssetName);
						}
						else
						{
							assetInfos.Add(genAssetInfo_Loader(staticSoftRef, assetGroupIndex, assetTypeID, assetName[i]));							
						}
					}
				}
			}
			break;
		}
	case 2:
		{
			for (int i = 0; i < assetName.Num(); ++i)
			{
				if (assetName[i].IsEmpty())
				{
					break;
				}
				if(groupToWorkWith->SoundWaves.Find(assetName[i]))
				{
					const auto soundSoftRef = groupToWorkWith->SoundWaves.FindRef(assetName[i]);
					auto l_index = SoftRef_SW.Find(soundSoftRef);
					if (!SoftRef_SW.Contains(soundSoftRef))
					{
						AssetInfo_SW.Add(genAssetInfo_SW(subscriber, assetGroupIndex, assetName[i]));
						SoftRef_SW.Add(soundSoftRef);
						assetInfos.Add(genAssetInfo_Loader(soundSoftRef, assetGroupIndex, assetTypeID, assetName[i]));
					}
					else
					{
						auto newSub = genSubscriberInfo(subscriber, assetGroupIndex, assetName[i]);
						if(!AssetInfo_SW[l_index].subscribers.Contains(newSub))
						{
							AssetInfo_SW[l_index].subscribers.Add(newSub);
						}
						if(AssetInfo_SW[l_index].hardReference)
						{
							int lType = 0;
							FString l_AssetName = assetName[i];							
							II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, lType, assetGroupIndex, l_AssetName);
						}
						else
						{
							assetInfos.Add(genAssetInfo_Loader(soundSoftRef, assetGroupIndex, assetTypeID, assetName[i]));							
						}
					}
				}
			}
			break;
		}
	case 3:
		{
			for (int i = 0; i < assetName.Num(); ++i)
			{
				if (assetName[i].IsEmpty())
				{
					break;
				}
				if(groupToWorkWith->Textures2D.Find(assetName[i]))
				{
					const auto texturesSoftRef = groupToWorkWith->Textures2D.FindRef(assetName[i]);
					auto l_index = SoftRef_TE.Find(texturesSoftRef);
					if (!SoftRef_TE.Contains(texturesSoftRef))
					{
						AssetInfo_TE.Add(genAssetInfo_TE(subscriber, assetGroupIndex, assetName[i]));
						SoftRef_TE.Add(texturesSoftRef);
						assetInfos.Add(genAssetInfo_Loader(texturesSoftRef, assetGroupIndex, assetTypeID, assetName[i]));
					}
					else
					{
						auto newSub = genSubscriberInfo(subscriber, assetGroupIndex, assetName[i]);
						if(!AssetInfo_TE[l_index].subscribers.Contains(newSub))
						{
							AssetInfo_TE[l_index].subscribers.Add(newSub);
						}
						if(AssetInfo_TE[l_index].hardReference)
						{
							int lType = 0;
							FString l_AssetName = assetName[i];							
							II_AssetsManager::Execute_AssetLoadedToMemory(subscriber, lType, assetGroupIndex, l_AssetName);
						}
						else
						{
							assetInfos.Add(genAssetInfo_Loader(texturesSoftRef, assetGroupIndex, assetTypeID, assetName[i]));							
						}
					}
				}
			}
			break;
		}

	}
	if(assetInfos.Num()>0)
	{
		RAM_load(subscriber, assetInfos);
	}
}

USkeletalMesh* UDA_AssetsManager::GetAsset_SK_Implementation(UObject* subscriber, int assetGroupIndex, const FString& assetName)
{
	USkeletalMesh* lAsset = nullptr;
	UDA_AssetsManager_AssetsGroup* groupToWorkWith = assetGroups[assetGroupIndex];

	if (assetName.IsEmpty())
	{
		return nullptr;
	}
	
	if(groupToWorkWith->SkeletalMeshes.Find(assetName))
	{
		const auto skeletSoftRef = groupToWorkWith->SkeletalMeshes.FindRef(assetName);
		auto l_index = SoftRef_SK.Find(skeletSoftRef);
		lAsset = AssetInfo_SK[l_index].hardReference;
	}
	return lAsset;
}

UStaticMesh* UDA_AssetsManager::GetAsset_SM_Implementation(UObject* subscriber, int assetGroupIndex,
	const FString& assetName)
{
	UStaticMesh* lAsset = nullptr;
	UDA_AssetsManager_AssetsGroup* groupToWorkWith = assetGroups[assetGroupIndex];

	if (assetName.IsEmpty())
	{
		return nullptr;
	}
	
	if(groupToWorkWith->StaticMeshes.Find(assetName))
	{
		const auto staticSoftRef = groupToWorkWith->StaticMeshes.FindRef(assetName);
		auto l_index = SoftRef_SM.Find(staticSoftRef);
		lAsset = AssetInfo_SM[l_index].hardReference;
	}
	return lAsset;
}

USoundWave* UDA_AssetsManager::GetAsset_SW_Implementation(UObject* subscriber, int assetGroupIndex,
	const FString& assetName)
{
	USoundWave* lAsset = nullptr;
	UDA_AssetsManager_AssetsGroup* groupToWorkWith = assetGroups[assetGroupIndex];

	if (assetName.IsEmpty())
	{
		return nullptr;
	}
	
	if(groupToWorkWith->SoundWaves.Find(assetName))
	{
		const auto soundSoftRef = groupToWorkWith->SoundWaves.FindRef(assetName);
		auto l_index = SoftRef_SW.Find(soundSoftRef);
		lAsset = AssetInfo_SW[l_index].hardReference;
	}
	return lAsset;
}

UTexture2D* UDA_AssetsManager::GetAsset_TE_Implementation(UObject* subscriber, int assetGroupIndex,
	const FString& assetName)
{
	UTexture2D* lAsset = nullptr;
	UDA_AssetsManager_AssetsGroup* groupToWorkWith = assetGroups[assetGroupIndex];

	if (assetName.IsEmpty())
	{
		return nullptr;
	}
	
	if(groupToWorkWith->Textures2D.Find(assetName))
	{
		const auto textureSoftRef = groupToWorkWith->Textures2D.FindRef(assetName);
		auto l_index = SoftRef_TE.Find(textureSoftRef);
		lAsset = AssetInfo_TE[l_index].hardReference;
	}
	return lAsset;
}

void UDA_AssetsManager::RemoveAsset_Implementation(UObject* subscriber, int assetTypeID, int assetGroupIndex, const FString& assetName)
{	
	UDA_AssetsManager_AssetsGroup* groupToWorkWith = assetGroups[assetGroupIndex];

	if (assetName.IsEmpty())
	{
		return;
	}
	
	switch (assetTypeID)
	{
	default:
		{
			break;
		}
	case 0:
		{
			if(groupToWorkWith->SkeletalMeshes.Find(assetName))
			{
				const auto skeletSoftRef = groupToWorkWith->SkeletalMeshes.FindRef(assetName);
				const auto l_index = SoftRef_SK.Find(skeletSoftRef);
				if (!AssetInfo_SK.IsValidIndex(l_index))
				{
					return;
				}
				const auto l_subIndex = AssetInfo_SK[l_index].subscribers.Find(genSubscriberInfo(subscriber, assetGroupIndex, assetName));
				if(!AssetInfo_SK[l_index].subscribers.IsValidIndex(l_subIndex))
				{
					return;
				}
				AssetInfo_SK[l_index].subscribers[l_subIndex].subscriber = nullptr;
				AssetInfo_SK[l_index].subscribers.RemoveAt(l_subIndex);
				if(AssetInfo_SK[l_index].subscribers.Num()<=0)
				{
					SoftRef_SK.RemoveAt(l_index);	
			
					//auto refToDelete = AssetInfo_SK[l_index].hardReference;
			
					//AssetInfo_SK[l_index].hardReference->ConditionalBeginDestroy();
					//AssetInfo_SK[l_index].hardReference->MarkPendingKill();				
					AssetInfo_SK[l_index].hardReference = nullptr;				
					assetLoader.Unload(skeletSoftRef.ToSoftObjectPath());
					//GEngine->ForceGarbageCollection(true);
					GEngine->PerformGarbageCollectionAndCleanupActors();			
					
					AssetInfo_SK.RemoveAt(l_index);
			
					//refToDelete->ConditionalBeginDestroy();
					//ConditionalBeginDestroy();
					//GEngine()->ForceGarbageCollection();
			
				}
			}
			break;
		}
	case 1:
		{
			if(groupToWorkWith->StaticMeshes.Find(assetName))
			{
				const auto staticSoftRef = groupToWorkWith->StaticMeshes.FindRef(assetName);
				const auto l_index = SoftRef_SM.Find(staticSoftRef);
				if (!AssetInfo_SM.IsValidIndex(l_index))
				{
					return;
				}
				const auto l_subIndex = AssetInfo_SM[l_index].subscribers.Find(genSubscriberInfo(subscriber, assetGroupIndex, assetName));
				if(!AssetInfo_SM[l_index].subscribers.IsValidIndex(l_subIndex))
				{
					return;
				}
				AssetInfo_SM[l_index].subscribers[l_subIndex].subscriber = nullptr;
				AssetInfo_SM[l_index].subscribers.RemoveAt(l_subIndex);
				if(AssetInfo_SM[l_index].subscribers.Num()<=0)
				{
					SoftRef_SM.RemoveAt(l_index);	
			
					//auto refToDelete = AssetInfo_SM[l_index].hardReference;
			
					//AssetInfo_SM[l_index].hardReference->ConditionalBeginDestroy();
					//AssetInfo_SM[l_index].hardReference->MarkPendingKill();				
					AssetInfo_SM[l_index].hardReference = nullptr;				
					assetLoader.Unload(staticSoftRef.ToSoftObjectPath());
					//GEngine->ForceGarbageCollection(true);
					GEngine->PerformGarbageCollectionAndCleanupActors();			
					
					AssetInfo_SM.RemoveAt(l_index);
			
					//refToDelete->ConditionalBeginDestroy();
					//ConditionalBeginDestroy();
					//GEngine()->ForceGarbageCollection();
			
				}
			}
			break;
		}
	case 2:
		{
			if(groupToWorkWith->SoundWaves.Find(assetName))
			{
				const auto soundSoftRef = groupToWorkWith->SoundWaves.FindRef(assetName);
				const auto l_index = SoftRef_SW.Find(soundSoftRef);
				if (!AssetInfo_SW.IsValidIndex(l_index))
				{
					return;
				}
				const auto l_subIndex = AssetInfo_SW[l_index].subscribers.Find(genSubscriberInfo(subscriber, assetGroupIndex, assetName));
				if(!AssetInfo_SW[l_index].subscribers.IsValidIndex(l_subIndex))
				{
					return;
				}
				AssetInfo_SW[l_index].subscribers[l_subIndex].subscriber = nullptr;
				AssetInfo_SW[l_index].subscribers.RemoveAt(l_subIndex);
				if(AssetInfo_SW[l_index].subscribers.Num()<=0)
				{
					SoftRef_SW.RemoveAt(l_index);	
			
					//auto refToDelete = AssetInfo_SW[l_index].hardReference;
			
					//AssetInfo_SW[l_index].hardReference->ConditionalBeginDestroy();
					//AssetInfo_SW[l_index].hardReference->MarkPendingKill();				
					AssetInfo_SW[l_index].hardReference = nullptr;				
					assetLoader.Unload(soundSoftRef.ToSoftObjectPath());
					//GEngine->ForceGarbageCollection(true);
					GEngine->PerformGarbageCollectionAndCleanupActors();			
					
					AssetInfo_SW.RemoveAt(l_index);
			
					//refToDelete->ConditionalBeginDestroy();
					//ConditionalBeginDestroy();
					//GEngine()->ForceGarbageCollection();
			
				}
			}
			break;
		}
	case 3:
		{
			if(groupToWorkWith->Textures2D.Find(assetName))
			{
				const auto textureSoftRef = groupToWorkWith->Textures2D.FindRef(assetName);
				const auto l_index = SoftRef_TE.Find(textureSoftRef);
				if (!AssetInfo_TE.IsValidIndex(l_index))
				{
					return;
				}
				const auto l_subIndex = AssetInfo_TE[l_index].subscribers.Find(genSubscriberInfo(subscriber, assetGroupIndex, assetName));
				if(!AssetInfo_TE[l_index].subscribers.IsValidIndex(l_subIndex))
				{
					return;
				}
				AssetInfo_TE[l_index].subscribers[l_subIndex].subscriber = nullptr;
				AssetInfo_TE[l_index].subscribers.RemoveAt(l_subIndex);
				if(AssetInfo_TE[l_index].subscribers.Num()<=0)
				{
					SoftRef_TE.RemoveAt(l_index);	
			
					//auto refToDelete = AssetInfo_TE[l_index].hardReference;
			
					//AssetInfo_TE[l_index].hardReference->ConditionalBeginDestroy();
					//AssetInfo_TE[l_index].hardReference->MarkPendingKill();				
					AssetInfo_TE[l_index].hardReference = nullptr;				
					assetLoader.Unload(textureSoftRef.ToSoftObjectPath());
					//GEngine->ForceGarbageCollection(true);
					GEngine->PerformGarbageCollectionAndCleanupActors();			
					
					AssetInfo_TE.RemoveAt(l_index);
			
					//refToDelete->ConditionalBeginDestroy();
					//ConditionalBeginDestroy();
					//GEngine()->ForceGarbageCollection();
			
				}
			}
			break;
		}
	}
}

void UDA_AssetsManager::ActorDestroy_Implementation(AActor* subscriber)
{
	subscriber->Destroy();
	GEngine->ForceGarbageCollection(true);
	
}
